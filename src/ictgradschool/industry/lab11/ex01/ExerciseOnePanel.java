package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JTextField heightInput;
    private JTextField weightInput;
    private JTextField bmiResult;
    private JButton calculateHealthyWeight;
    private JTextField healthyWeightResult;
    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        calculateBMIButton = new JButton("Calculate BMI");
        calculateHealthyWeight = new JButton("Calculate Healthy Weight");
        heightInput = new JTextField(15);
        weightInput = new JTextField(15);
        bmiResult = new JTextField(15);
        healthyWeightResult = new JTextField(15);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel yourHeight = new JLabel("Height in metres: ");
        JLabel yourWeight = new JLabel("Weight in kilograms: ");
        JLabel yourBMI = new JLabel("Your Body Mass Index (BMI) is: ");
        JLabel healthyWeight = new JLabel("Maximum Healthy Weight for your Height: ");

        // TODO Add JLabels, JTextFields and JButtons to window
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(yourHeight);
        this.add(heightInput);
        this.add(yourWeight);
        this.add(weightInput);
        this.add(calculateBMIButton);
        this.add(yourBMI);
        this.add(bmiResult);
        this.add(calculateHealthyWeight);
        this.add(healthyWeight);
        this.add(healthyWeightResult);

        // TODO Add Action Listeners for the JButtons

        calculateBMIButton.addActionListener(this);
        calculateHealthyWeight.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        double height  = Double.parseDouble(heightInput.getText());
        if (event.getSource() == calculateBMIButton) {
            double weight  = Double.parseDouble(weightInput.getText());
            double BMI = roundTo2DecimalPlaces(weight/(height*height));
            bmiResult.setText(Double.toString(BMI));
        }else if (event.getSource() == calculateHealthyWeight) {
            double maxHealthyWeight = roundTo2DecimalPlaces(24.9 * height * height);

            healthyWeightResult.setText(Double.toString(maxHealthyWeight));
        }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}