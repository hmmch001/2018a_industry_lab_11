package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private  Balloon balloon;
    public ArrayList<Balloon> balloons;
    //private  JButton moveButton;
    private Timer timer;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);
        this.balloons = new ArrayList<>();

        this.balloon = new Balloon(30, 60);
        this.balloons.add(balloon);
        //this.moveButton = new JButton("Move balloon");
        //this.moveButton.addActionListener(this);
        //this.add(moveButton);
        this.addKeyListener(this);
        this.timer = new Timer(200, this);
    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        //balloon.move();
        for (Balloon balloon  : this.balloons) {

                balloon.move();

        }


        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //balloon.draw(g);
        for (Balloon balloon : this.balloons) {
            balloon.draw(g);
        }
        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    public void keyPressed(KeyEvent e){
        Balloon newBalloon = new Balloon(((int)(Math.random()*800)),((int)(Math.random()*800)));
        this.balloons.add(newBalloon);
        for(Balloon balloon : this.balloons) {
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                balloon.setDirection(Direction.Up);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                balloon.setDirection(Direction.Left);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                balloon.setDirection(Direction.Down);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                balloon.setDirection(Direction.Right);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_S) {
                timer.stop();
            }
        }

    }

    public void keyTyped(KeyEvent e){

    }

    public void keyReleased(KeyEvent e){

    }
}