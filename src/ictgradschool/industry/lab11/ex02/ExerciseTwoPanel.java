package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */

public class ExerciseTwoPanel extends JPanel implements ActionListener{

    /**
     * Creates a new ExerciseFivePanel.
     */
    private JButton add;
    private JButton subtract;
    private JTextField num1;
    private JTextField num2;
    private JTextField result;


    public ExerciseTwoPanel() {
        setBackground(Color.white);
        add = new JButton("Add");
        subtract = new JButton("Subtract");
        num1 = new JTextField(10);
        num2 = new JTextField(10);
        result = new JTextField(20);
        JLabel resultLabel = new JLabel("Result: ");

        this.add(num1);
        this.add(num2);
        this.add(add);
        this.add(subtract);
        this.add(resultLabel);
        this.add(result);

        add.addActionListener(this);
        subtract.addActionListener(this);

    }
    public void actionPerformed(ActionEvent event) {
        double numA  = Double.parseDouble(num1.getText());
        double numB  = Double.parseDouble(num2.getText());
        double answer;
        if (event.getSource() == add) {
            answer = roundTo2DecimalPlaces(numA + numB);
            result.setText(Double.toString(answer));
        } else if (event.getSource() == subtract) {
            answer = roundTo2DecimalPlaces(numA - numB);
            result.setText(Double.toString(answer));
        }

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}